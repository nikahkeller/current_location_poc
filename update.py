import math
from multiprocessing import Pool
from random import randint

from decouple import config
from google.cloud import firestore
import pygeohash as pgh


project = config('PROJECT_ID')
db = firestore.Client(project=project)


def get_asset(doc_ref):
    data = doc_ref.get().to_dict()
    asset = data['d']
    return asset


def get_random_asset_ref():
    key = randint(1, 999)
    asset_ref = db.collection('assets').document(str(key))
    return asset_ref


def calculate_new_location_for_asset(asset, heading):
    move_by = 0.005
    new_lat = move_by * math.sin(heading) + asset['latitude']
    new_lng = move_by * math.cos(heading) + asset['longitude']
    return new_lat, new_lng


def update_asset_location():
    asset_ref = get_random_asset_ref()
    asset = get_asset(asset_ref)
    new_heading = randint(1, 360)
    new_lat, new_lng = calculate_new_location_for_asset(asset, new_heading)
    new_location = firestore.GeoPoint(new_lat, new_lng)
    geohash = pgh.encode(new_lat, new_lng)
    asset_ref.update({
        'g': geohash,
        'l': new_location,
        'd.heading': new_heading,
        'd.latitude': new_lat,
        'd.longitude': new_lng
    })


if __name__ == '__main__':
    with Pool(20) as p:
        while True:
            p.apply_async(update_asset_location)

