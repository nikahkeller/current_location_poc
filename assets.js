let map;
let config = {
    apiKey: "{example_key}",
    databaseURL: "{example_url}",
    projectId: "{example_project_id}"
};
firebase.initializeApp(config);
let firestore = firebase.firestore();
const geoFirestore = new GeoFirestore(firestore);
const geoCollectionRef = geoFirestore.collection('assets');
let subscription;
const markers = {};
let center = {lat: 47.6062, lng: -122.3321};
let positions = [
    {lat: 47.622103, lng: -122.322400},
    {lat: 47.622450, lng: -122.284387},
    {lat: 47.593753, lng: -122.288948},
    {lat: 47.593744, lng: -122.332767},
    {lat: 47.622103, lng: -122.322400}
];

let zone;
let circle;
let mode;

function realTime() {
    mode = 'radius';
    const radius = 20;
    circle = new google.maps.Circle({
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.05,
        center: center,
        radius: radius * 1000
    });
    circle.setMap(map);
    let marker = new google.maps.Marker({
        position: center,
        title:"Center!",
        icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'
    });
    marker.setMap(map);
    queryFirestore(center, radius, mode);
}

function assetsZone() {
    mode = 'zone';
    zone = new google.maps.Polygon({
        paths: positions,
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.05
    });
    zone.setMap(map);

    let points = [
        [47.622103, -122.322400],
        [47.622450, -122.284387],
        [47.593753, -122.288948],
        [47.593744, -122.332767]
    ];
    let sumX = 0;
    let sumY = 0;
    let geoPoints = [];


    for (let point of points) {
        sumX += point[0];
        sumY += point[1];
        geoPoints.push(turf.point([point[0], point[1]]))
    }
    let avgX = sumX / points.length;
    let avgY = sumY / points.length;
    let avgPoint = turf.point([avgX, avgY]);
    let max = 0;
    for (let i = 0; i < geoPoints.length; i++) {
        let distance = turf.distance(avgPoint, geoPoints[i], {units: 'kilometers'});
        if (distance > max) {
            max = distance
        }
    }
    let center = {lat: avgPoint.geometry.coordinates[0], lng: avgPoint.geometry.coordinates[1]};
    let radius = max;

    let marker = new google.maps.Marker({
        position: center,
        title: "Center!",
        icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'
    });
    marker.setMap(map);
    queryFirestore(center, radius, mode);
}


function assetsRadius() {
    mode = 'radius';
    const radius = parseInt(document.getElementById('radius').value);
    circle = new google.maps.Circle({
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.05,
        center: center,
        radius: radius * 1000
    });
    circle.setMap(map);
    let marker = new google.maps.Marker({
        position: center,
        title:"Center!",
        icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'
    });
    marker.setMap(map);
    queryFirestore(center, radius, mode);
}

// Query assets' locations from Firestore
function queryFirestore(location, radius, mode) {
    if (subscription) {
        console.log('Old query subscription cancelled');
        subscription();
        subscription = false;
    }
    const query = geoCollectionRef.near({
        center: new firebase.firestore.GeoPoint(location.lat, location.lng),
        radius
    });
    console.log('New query subscription created');
    subscription = query.onSnapshot((snapshot) => {
        snapshot.docChanges().forEach((change) => {
            let doc = change.doc;
            let data = doc.data();
            switch (change.type) {
                case 'added':
                    console.log('Snapshot detected added marker');
                    return addMarker(doc.id, data);
                case 'modified':
                    console.log('Snapshot detected modified marker');
                    return updateMarker(doc.id, data);
                case 'removed':
                    console.log('Snapshot detected removed marker');
                    return removeMarker(doc.id, data);
                default:
                    break;
            }
        });
    });
}


// Add Marker to Google Maps
function addMarker(key, data) {
    let marker_config = {
        position: {lat: data.latitude, lng: data.longitude},
        title:"Center!",
        icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'
    };
    if (!markers[key]) {
        if (mode === 'zone') {
            let position = new google.maps.LatLng(data.latitude, data.longitude);
            if (google.maps.geometry.poly.containsLocation(position, zone)) {
                markers[key] = new google.maps.Marker(marker_config);
                markers[key].setMap(map)
            }
        } else {
            markers[key] = new google.maps.Marker(marker_config);
            markers[key].setMap(map)
        }
    }
}


// Remove Marker from Google Maps
function removeMarker(key) {
    if (markers[key]) {
        markers[key] = null;
        delete markers[key];
    }
}


// Update Marker on Google Maps
function updateMarker(key, data) {
    if (markers[key]) {
        markers[key].setPosition({
            lat: data.latitude,
            lng: data.longitude
        });
    } else {
        addMarker(key, data);
    }
}

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: center,
        zoom: 10
    });
}