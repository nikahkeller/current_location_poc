from random import getrandbits, randint, uniform

from decouple import config
from google.cloud import firestore
import pygeohash as pgh


project = config('PROJECT_ID')
db = firestore.Client(project=project)


def insert():
    for i in range(1, 1000):
        print(i)
        division_id = randint(1, 100)
        company_id = randint(1, 1000)
        asset_id = i
        legacy_account_id = randint(1, 100)
        poweron = bool(getrandbits(1))
        idling = bool(getrandbits(1))
        heading = randint(1, 360)
        odometer = randint(1, 500000)
        speed = randint(50, 60)
        latitude = uniform(46.606362, 49.3457868)
        longitude = uniform(-123.7844079, -121.9513812)
        location = firestore.GeoPoint(latitude, longitude)
        geohash = pgh.encode(latitude, longitude)
        data = {
            'g': geohash,
            'l': location,
            'd': {
                'division_id': division_id,
                'company_id': company_id,
                'asset_id': asset_id,
                'legacy_account': legacy_account_id,
                'legacy_asset_id': asset_id,
                'poweron': poweron,
                'idling': idling,
                'heading': heading,
                'odometer': odometer,
                'speed': speed,
                'latitude': latitude,
                'longitude': longitude
            }
        }

        db.collection('assets').document(str(asset_id)).set(data)


insert()
